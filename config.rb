http_path = "/" # определяет путь к корню
css_dir = "/" # определяет путь к сss
sass_dir = "/" # определяет путь к sass 
images_dir = "img" # определяет путь к каталогу изображений
javascripts_dir = "js" # определяет путь к каталогу JavaScript 
# Предпочитаемый стиль вывода:
# output_style = :expanded или :nested или :compact или :compressed
# Разрешить относительные пути. 
relative_assets = true